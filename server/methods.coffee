proffessionsToBuilding =
  'полицейский': 'участок'
  'продавец': 'магазин'
  'монах': 'церковь'

Meteor.methods
  createGoomba: () ->
    sex = Random.choice(['male', 'female'])
    goombaId = Goombas.insert(
      is_player: false
      x:7*30
      y:7*30
      name: 'test'
      avatar: 'http://api.adorable.io/avatars/' + Random.id()
      occupation: Random.choice(['рабочий', 'полицейский', 'продавец', 'безработный', 'монах'])
      personality: Random.choice(['ENTJ', 'ESFP', 'ISTP', 'INTJ', 'INFP'])
      aligment: Random.choice(['злой', 'добрый', 'нейтральный'])
      sex: sex
      health: GUEST_LIFE_INITIAL
      hunger: GUEST_HUNGER_INITIAL
      thirst: GUEST_THIRST_INITIAL
    )
    if sex == 'female'
      url = 'https://uinames.com/api?gender=female&country=england'
    else
      url = 'https://uinames.com/api?gender=male&country=england'
    func = ((goombaId) ->
      Meteor.http.get url, (err, res) ->
        res = JSON.parse(res.content)
        Goombas.update goombaId, $set: name: res['name'] + ' ' + res['surname']
        console.info "The name is #{res['name']}."
        turn = GameSession.findOne()?.turn
        Events.insert
          create_time: new Date().getTime(),
          who: goombaId,
          desc: 'arrives'
          turn: turn
    ).bind(this, goombaId)
    func()
    return goombaId

  getRandomEvents: () ->
    console.log 'getRandomEvents'
    # check how bad things are
    # select event
    # select object
    decayedCorpses = Goombas.find({health: {$lte: 0}, is_decayed: true, not_show: {$ne: true}}).count()
    console.log decayedCorpses/DESIASE_COEFF, Random.fraction(), Random.fraction()*(decayedCorpses/DESIASE_COEFF)
    if decayedCorpses and Random.fraction()*(decayedCorpses/DESIASE_COEFF)
      illGoomba = Goombas.findOne(health: {$gt: 0},  not_show: {$ne: true}, is_poisoned: {$ne: true})
      if illGoomba
        Goombas.update illGoomba._id, $set: is_poisoned: true
        turn = GameSession.findOne()?.turn
        Events.insert
          create_time: new Date().getTime(),
          who: illGoomba._id,
          desc: "#{illGoomba.name} got a desease"
          turn: turn

    events = RandomEvents.find(condition: $exists: false).fetch()
    for event in RandomEvents.find(condition: $exists: true).fetch()
      console.log(event.condition)
      if eval event.condition
        events.push(event)
    console.log 'events', events
    events.sort((a,b) -> a.was_used - b.was_used)

    # random select from 1st half - most uncommon
    event = Random.choice(events.splice(0, events.length/2))
    obj = null
    if event
      #update event was_used
      RandomEvents.update(event._id, $inc: was_used: 1)

      #select obj for event
      evaluatedContext = eval event.context
      if evaluatedContext
        if event.exclude_self
          goombaId = GameSession.findOne()?.goomba_id
          if goombaId
            context = _.filter(evaluatedContext, (itm) -> itm._id != goombaId)
          else
            context = eval(evaluatedContext)
        else
          context = eval(evaluatedContext)
        obj = Random.choice(context)

    return [event, obj]

  updateHungerAndThirst: () ->
    console.log 'updateHungerAndThirst'

    Goombas.update { health: {$gt: 0}, hunger: $gt: 0 }, { $inc:
      hunger: -HUNGER_PER_DAY }, { multi: true }, ->
      console.log 'args', arguments

    Goombas.update { health: {$gt: 0}, thirst: $gt: 0 }, { $inc:
      thirst: -THIRST_PER_DAY }, { multi: true }, ->
      console.log 'args', arguments

    Goombas.update { health: {$gt: 0}, hunger: $lt: 1 },
      {
        $inc: {health: -HUNGER_PER_DAY_DAMAGE},
        $set: damage_type: 'hunger'
      }, { multi: true }, ->
      console.log 'args', arguments

    Goombas.update { health: {$gt: 0}, thirst: $lt: 1 },
      {
        $inc: {health: -HUNGER_PER_DAY_DAMAGE},
        $set: damage_type: 'thirst'
      }, { multi: true }, ->
      console.log 'args', arguments

  drownObject: () ->
    console.log 'drownObject'

    Goombas.update {not_show: {$ne: true}, $or: [{x: 7*30, y: 9*30}, {x: 8*30, y: 9*30}, {x: 9*30, y: 9*30}, {x: 9*30, y: 8*30}, {x: 9*30, y: 7*30}, {x: 8*30, y: 8*30}]}, { $inc:
      days_in_sea: DAYS_IN_SEA_PER_DAY }, { multi: true }, ->
      console.log 'args', arguments


  breedAnimalsOnTheWild: () ->
    Goombas.update {health: {$gt: 0}, is_animal: true, is_catched: {$ne: true}}, { $inc: days_in_wild: DAYS_IN_WILD_PER_DAY }, { multi: true }

    if Goombas.find({health: {$gt: 0}, is_animal: true, is_catched: {$ne: true}, days_in_wild: {$gte: BREED_DAYS_IN_WILD}}).count() >= BREED_UNTIS_IN_WILD
      gs = GameSession.findOne()

      goombaId = Goombas.insert(
        x: 2*30
        y: 1*30
        name: "pig #{gs.pig_number}"
        avatar: 'http://downloadicons.net/sites/default/files/pink-pig-icon-14955.png'
        sex: Random.choice(['male', 'female'])
        health: PIG_LIFE_INITIAL
        hunger: PIG_HUNGER_INITIAL
        thirst: PIG_THIRST_INITIAL
        is_animal: true
      )
      GameSession.update(gs._id, {$inc: pig_number: 1})
      turn = gs?.turn
      Events.insert
        create_time: new Date().getTime(),
        who: goombaId,
        desc: 'new pig is born in the wild'
        turn: turn

  breedAnimalsOnTheFarm: () ->
    Goombas.update {health: {$gt: 0}, is_animal: true, is_catched: true}, { $inc: days_in_farm: DAYS_IN_FARM_PER_DAY }, { multi: true }

    if Goombas.find({health: {$gt: 0}, is_animal: true, is_catched: true, days_in_farm: {$gte: BREED_DAYS_IN_FARM}}).count() >= BREED_UNTIS_IN_FARM
      gs = GameSession.findOne()
      goombaId = Goombas.insert(
        x: 2*30
        y: 4*30
        name: "pig #{gs.pig_number}"
        avatar: 'http://downloadicons.net/sites/default/files/pink-pig-icon-14955.png'
        sex: Random.choice(['male', 'female'])
        health: PIG_LIFE_INITIAL
        hunger: PIG_HUNGER_INITIAL
        thirst: PIG_THIRST_INITIAL
        is_animal: true
        is_catched: true
      )
      GameSession.update(gs._id, {$inc: pig_number: 1})
      turn = gs?.turn
      Events.insert
        create_time: new Date().getTime(),
        who: goombaId,
        desc: 'new pig is born in the farm'
        turn: turn

  corpseDecay: () ->
    Goombas.update {health: {$lte: 0}, not_show: {$ne: true}}, { $inc: days_dead: DAYS_DEAD_PER_DAY }, { multi: true }

    if Goombas.find({health: {$lte: 0}, days_dead: {$gte: DAYS_BEFORE_DECAY}, is_decayed: {$ne: true}}).count()
      turn = GameSession.findOne()?.turn
      for goomba in Goombas.find({health: {$lte: 0}, days_dead: {$gte: DAYS_BEFORE_DECAY}, is_decayed: {$ne: true}}).fetch()
        Goombas.update goomba._id, { $set: is_decayed: true },
        Events.insert
          create_time: new Date().getTime(),
          who: goomba._id,
          desc: "#{goomba.name} starts decaying"
          turn: turn

  diseaseSpread: () ->
    Goombas.update {health: {$gt: 0}, not_show: {$ne: true}, is_poisoned: true}, { $inc: health: -DESIASE_DAMAGE_PER_DAY }, { multi: true }


  recreateGame: (options) ->
    GameSession.remove({})
    Tiles.remove({})
    Objects.remove({})
    Goombas.remove({})
    Buildings.remove({})
    Actions.remove({})
    Events.remove({})
    GameCase.remove({})
    Interrogation.remove({})
    RandomEvents.remove({})


    Actions.insert
      value: 'remove_corpse'
      text: 'remove corpse'
      cost: 10
      condition: 'Goombas.find({health: {$lte: 0}, not_show: {$ne: true}}).count()'
      context: 'Goombas.find({health: {$lte: 0}, not_show: {$ne: true}}).fetch()'
    Actions.insert
      value: 'catch_an_animal'
      text: 'catch an animal'
      cost: 4
      condition: 'Goombas.find({health: {$gt: 0}, is_animal: true, is_catched: {$ne: true}, not_show: {$ne: true}}).count()'
    # 4 for full
#    Actions.insert
#      value: 'repair_farm'
#      text: 'repair farm'
#      cost: 10
#      condition: 'Buildings.find({health: {$lt: 100}, type: \'ферма\'}).count()'
    # 2 for full
    Actions.insert
      value: 'repair_well'
      text: 'repair a well'
      cost: 10
      condition: 'Buildings.find({health: {$lt: 100}, type: \'колодец\'}).count()'
    # 2 for full
#    Actions.insert
#      value: 'repair_house'
#      text: 'repair a house'
#      cost: 10
#      condition: 'Buildings.find({health: {$lt: 100}, type: \'дом\'}).count()'

    Actions.insert
      value: 'attack'
      text: 'attack'
      cost: 3
      condition: 'Goombas.find({health: {$gt: 0}, not_show: {$ne: true}}).count()'
      context: 'Goombas.find({health: {$gt: 0}, not_show: {$ne: true}}).fetch()'
      exclude_self: true

    Actions.insert
      value: 'cook'
      text: 'cook'
      cost: 4
      condition: 'Goombas.find({health: {$lte: 0}, not_show: {$ne: true}}).count()'
      context: 'Goombas.find({health: {$lte: 0}, not_show: {$ne: true}}).fetch()'

    Actions.insert
      value: 'hunt'
      text: 'hunt'
      cost: 6

#    Actions.insert
#      value: 'cut_wood'
#      text: 'cut the wood'
#      cost: 3


    Actions.insert
      value: 'search_island'
      text: 'search an island'
      cost: 10

    Actions.insert
      value: 'clean_a_river'
      text: 'clean a river'
      cost: 5
      condition: 'Buildings.find({health: {$lt: 100}, type: \'river\'}).count()'

#    Actions.insert
#      value: 'repair_road'
#      text: 'repair a road'
#      cost: 10

    Actions.insert
      value: 'get_water_from_river'
      text: 'get water from river'
      cost: 5

    Actions.insert
      value: 'get_water_from_well'
      text: 'get water from well'
      cost: 3
      condition: 'Buildings.find({health: {$gte: 100}, type: \'колодец\'}).count()'

#    TODO hide?
    Actions.insert
      value: 'use_medkit'
      text: 'use medkit'
      condition: 'Objects.find({type: \'аптечка\', in_inventory: true, is_destroyed: false}).count()'
      cost: 8
#    TODO hide?
    Actions.insert
      value: 'eat_food'
      text: 'eat food'
      condition: 'Objects.find({type: \'еда\', in_inventory: true, is_destroyed: false}).count()'
      cost: 8
#    TODO hide?
    Actions.insert
      value: 'drink_water'
      text: 'drink water'
      condition: 'Objects.find({type: \'вода\', in_inventory: true, is_destroyed: false}).count()'
      cost: 8
    # Random events actions - will not be available for user
    Actions.insert
      value: 'decrease_thirst'
      text: 'decrease thirst'
      condition: 'false' # this action is for random event. We don't want user to execute it
    Actions.insert
      value: 'gone_mad'
      text: 'gone_mad'
      condition: 'false' # this action is for random event. We don't want user to execute it

    #
    RandomEvents.insert
      action: 'decrease_thirst'
      title: 'bad dream'
      description: '${goomba} had a nightmare. Now he feels thirsty.'
      condition: "Goombas.find({health: {$gt: 0}, not_show: {$ne: true}, is_animal: {$ne: true}, thirst: {$gt: #{THIRST_BAD_DREAM_EVENT}}}).count()"
      context: "Goombas.find({health: {$gt: 0}, not_show: {$ne: true},  is_animal: {$ne: true}, thirst: {$gt: #{THIRST_BAD_DREAM_EVENT}}}).fetch()"
      was_used: 0

    RandomEvents.insert
      action: 'gone_mad'
      title: 'Gone mad!'
      description: '${goomba} gone mad. He wanders away to the forest.'
      condition: 'Goombas.find({health: {$gt: 0}, not_show: {$ne: true}, is_animal: {$ne: true}}).count() > 1'
      context: 'Goombas.find({health: {$gt: 0}, not_show: {$ne: true}, is_animal: {$ne: true}}).fetch()'
      was_used: 0


    RandomEvents.insert
      action: 'new_found'
      title: 'New found!'
      description: '${goomba} found ${object}.'
      condition: 'Goombas.find({health: {$gt: 0}, not_show: {$ne: true}, is_animal: {$ne: true}}).count() > 1'
      context: 'Goombas.find({health: {$gt: 0}, not_show: {$ne: true}, is_animal: {$ne: true}}).fetch()'
      was_used: 0

    GameSession.insert
      turn: 0
      pig_number: 1
      inventory_size: INVENTORY_SIZE

    map = [
      ['g', 'g', 'g', 'g', 'g', 'r', 'g', 'g', 'g', 'g'],
      ['g', 'g', 'g', 'g', 'g', 'r', 'r', 'g', 'g', 'g'],
      ['d', 'g', 'g', 'g', 'g', 'g', 'r', 'r', 'r', 'g'],
      ['d', 'g', 'g', 'g', 'w', 'g', 'g', 'g', 'r', 'r'],
      ['g', 'g', 'p', 'p', 'g', 'h', 'g', 'g', 'g', 'g'],
      ['d', 'g', 'p', 'p', 'g', 'g', 'g', 'g', 'g', 'g'],
      ['g', 'g', 'g', 'g', 'g', 'h', 'g', 'h', 'g', 'g'],
      ['d', 'd', 'g', 'd', 'g', 'g', 'g', 'g', 'g', 's'],
      ['g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 's', 's'],
      ['g', 'g', 'g', 'g', 'g', 'g', 'g', 's', 's', 's'],
    ]

    if Tiles.find().count() == 0
      for i in [0..9]
        for j in [0..9]
          tileId = Tiles.insert(
            x: j*30
            y: i*30
            color: if map[i][j] == 'r' then 'blue' else if map[i][j] == 's' then 'darkblue' else if map[i][j] == 'h' then 'brown' else if map[i][j] == 'w' then 'darkbown' else if map[i][j] == 'p' then 'yellow' else if map[i][j] == 'd' then 'gray' else 'green'
          )
          if map[i][j] == 'h'
            Buildings.insert(
              x: j*30
              y: i*30
              tile_id: tileId
              type: 'дом'
              health: 35
            )
          else if map[i][j] == 'w'
            Buildings.insert(
              x: j*30
              y: i*30
              tile_id: tileId
              type: 'колодец'
              health: 35
            )
          else if map[i][j] == 'p'
            Buildings.insert(
              x: j*30
              y: i*30
              tile_id: tileId
              type: 'ферма'
              health: 35
            )

    for i in [0..5]
      [type, url] = Random.choice([
        ['аптечка', 'http://s1.iconbird.com/ico/0612/vistabasesoftwareicons/w64h641339252609FirstAidKit3.png'],
#        ['топор', 'http://s.leroymerlin.ru/upload/catalog/img/3/b/14262562/128x128/14262562.jpg?v=6'],
#        ['факел', 'https://rust-wiki.com/images/b/b2/Torch.png'],
        ['еда', 'https://www.shareicon.net/data/128x128/2016/09/26/834904_food_512x512.png'],
        ['вода', 'http://icons.veryicon.com/256/System/Line/Glass%20Water.png'],
      ])
      Objects.insert(
        x: Random.choice([0..9])*30
        y: Random.choice([0..9])*30
        type: type
        in_inventory: false
        is_destroyed: false
        avatar: url
      )

    sex = Random.choice(['male', 'female'])
    goombaId = Goombas.insert(
      is_player: true
      x:0
      y:0
      name: 'test'
      avatar: 'http://api.adorable.io/avatars/' + Random.id()
      occupation: Random.choice(['рабочий', 'полицейский', 'продавец', 'безработный', 'монах'])
      personality: Random.choice(['ENTJ', 'ESFP', 'ISTP', 'INTJ', 'INFP'])
      aligment: Random.choice(['злой', 'добрый', 'нейтральный'])
      sex: sex
      health: PLAYER_LIFE_INITIAL
      hunger: PLAYER_HUNGER_INITIAL
      thirst: PLAYER_THIRST_INITIAL
    )
    if sex == 'female'
      url = 'https://uinames.com/api?gender=female&country=england'
    else
      url = 'https://uinames.com/api?gender=male&country=england'
    func = ((goombaId) ->
      Meteor.http.get url, (err, res) ->
        res = JSON.parse(res.content)
        Goombas.update goombaId, $set: name: res['name'] + ' ' + res['surname']
        return
    ).bind(this, goombaId)
    func()

    # создаем трупы
    for i in [0..5]
      goombaId = Goombas.insert(
        x: Random.choice([0..9])*30
        y: Random.choice([0..9])*30
        name: 'test'
        avatar: 'http://api.adorable.io/avatars/' + Random.id()
        occupation: Random.choice(['рабочий', 'полицейский', 'продавец', 'безработный', 'монах'])
        personality: Random.choice(['ENTJ', 'ESFP', 'ISTP', 'INTJ', 'INFP'])
        aligment: Random.choice(['злой', 'добрый', 'нейтральный'])
        sex: Random.choice(['male', 'female'])
        health: 0
        hunger: 0
        thirst: 0
      )

      if sex == 'female'
        url = 'https://uinames.com/api/?gender=female&country=england'
      else
        url = 'https://uinames.com/api/?gender=male&country=england'
      func = ((goombaId) ->
        Meteor.http.get url, (err, res) ->
          res = JSON.parse(res.content)
          Goombas.update goombaId, $set: name: res['name'] + ' ' + res['surname']
          return
      ).bind(this, goombaId)
      func()
    gs = GameSession.findOne()
    # создаем зверей - свиней
    pigNumber = gs.pig_number
    for i in [0..2]
      goombaId = Goombas.insert(
        x: Random.choice([0..9])*30
        y: Random.choice([0..9])*30
        name: "pig #{pigNumber}"
        avatar: 'http://downloadicons.net/sites/default/files/pink-pig-icon-14955.png'
        sex: Random.choice(['male', 'female'])
        health: PIG_LIFE_INITIAL
        hunger: PIG_HUNGER_INITIAL
        thirst: PIG_THIRST_INITIAL
        is_animal: true
      )
      GameSession.update(gs._id, {$inc: pig_number: 1})
      pigNumber++

    goombasIds = Goombas.find( health: {$gt: 0}, is_animal: {$ne: true}, not_show: {$ne: true} ).map( (s) -> s._id)
    if not gs.goomba_id
      goombaId = goombasIds[0]

    if goombaId
      GameSession.update(gs._id, {$set: {goomba_id: goombaId},  $unset: {spend_points: 1}})

    Events.insert
      create_time: new Date().getTime(),
      desc: 'While you were out your home island had been washed by a super giant wave.
 It looks like everyone is dead. The buidings are damaged. The farm is broken and the pigs are roaming freely on the island.'

#выбираем дом
#      freeBuildings = Buildings.find( type: 'дом', lives : $size : 0).fetch()

#      unless freeBuildings
#        freeBuildings = Buildings.find( type: 'дом',  lives : $size : 1).fetch()
#      home = Random.choice(freeBuildings)

#      Goombas.update goombaId, $set: lives_at: home._id
#      Buildings.update home._id, $push: lives: goombaId

#        #выбираем работу
#        goomba = Goombas.findOne(goombaId)
#        unless goomba.occupation == 'безработный'
#          if goomba.occupation  in Object.keys(proffessionsToBuilding)
#            buildingType = proffessionsToBuilding[goomba.occupation]
#            building = Random.choice(Buildings.find(type: buildingType).fetch())
#          else
#            building = Random.choice(Buildings.find(type: $ne: 'дом').fetch())
#
#          Goombas.update goombaId, $set: works_at: building._id
#          Buildings.update building._id, $push: works: goombaId