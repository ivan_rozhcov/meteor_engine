Goombas.after.update (userId, doc, fieldNames, modifier, options) ->
  #если код поменялся удаляем старый код
  turn = GameSession.findOne()?.turn

  if doc.health != this.previous.health and doc.health <= 0
    Events.insert
      create_time: new Date().getTime(),
      who: doc._id,
      desc: "died from #{doc.damage_type}"
      turn: turn
  if doc.days_in_sea != this.previous.days_in_sea and doc.days_in_sea >=  MAX_DAYS_IN_SEA
    eventId = Events.insert
      create_time: new Date().getTime(),
      who: doc._id,
      desc: "drown in sea"
      turn: turn
    Goombas.direct.update doc._id, $set: {not_show: true, reason: eventId}
