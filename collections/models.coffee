@Goombas = new Meteor.Collection("goombas")

@Tiles = new Meteor.Collection("tiles")

@Buildings = new Meteor.Collection("buildings")

@Objects = new Meteor.Collection("objects")

@Actions = new Meteor.Collection("actions")

@RandomEvents = new Meteor.Collection("random_events")

@Events = new Meteor.Collection("events")

@GameSession = new Meteor.Collection("game_session")

#TODO remove
@GameCase = new Meteor.Collection("game_case")

#TODO remove
@Interrogation = new Meteor.Collection("interrogation")
