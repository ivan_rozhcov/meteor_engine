Template.grid.events
  'click .goomba': ->
#    alert("нажал на картинку #{@name}")
    Session.set "goombaForDialog", @_id
    Session.set "currentGoombaId", @_id
    $( "#goomba" ).dialog().show()

  'click rect[style~="fill:yellow;"]': ->
    console.log 'dialog'
    Session.set "buildingForDialog", @_id
    $( "#building" ).dialog().show()


Template.grid.helpers
  objects: ->
    Objects.find(in_inventory: false, is_destroyed: false)
  tiles: ->
    Tiles.find()
  goombas: ->
    Goombas.find(not_show: $ne: true)
  buildings: ->
    Buildings.find()

Template.grid.onRendered ->
  Tracker.autorun ->
    iter = Goombas.find(not_show: {$ne: true}).map((g) -> x: g.x, y: g.y, id: g._id)
#    console.log iter
    obj = _.groupBy iter, (num) ->
      [
        num.x
        num.y
      ]
    Session.get "currentGoombaId"
    gs = GameSession.findOne()
    if gs
      gs.goomba_id
    for key in Object.keys(obj)
      if obj[key].length > 1
        width = 30/obj[key].length
        for gomb, index in obj[key]
          for svg in  $('#'+gomb.id).children()
            svg.setAttribute('width', width)
            svg.setAttribute('x', gomb.x + index*width)
      else
        for svg in  $('#'+obj[key][0].id).children()
          svg.setAttribute('width', 30)
          svg.setAttribute('x', obj[key][0].x)
    return

Template.goomba.helpers
  isDead: ->
    if @is_player and @health <= 0
      alert "game over. Player #{@name} is dead."
      GameSession.update(GameSession.findOne()._id, $set: is_gameover: true)
    @health <= 0

#onRendered ?
Template.gameStatus.onCreated ->
  query = GameSession.find()
  query.observeChanges
    changed: (id, fields) ->
  # можно конечно в хуки выносить, но зачем - тут проще реакции делать
      if fields?.turn

        if fields?.turn is 0
          turn = GameSession.findOne()?.turn
          Events.insert
            create_time: new Date().getTime(),
            desc: 'It\'s your first day on this island. You are starving.'
            turn: turn

        if fields?.turn is 2
          # BUG in meteor https://github.com/meteor/meteor/issues/907#issuecomment-48604374
          setTimeout( ->
            Meteor.call('createGoomba', (
              (error, goombaId) ->
                if goombaId and Session.get('ShowTutorial') and Session.get('ShowTutorialResource')  and not Session.get('ShowTutorialNewGoomba')
                  # Instance the tour
                  console.log 'create tour'
                  tour = new Tour(
                    steps: [
                      {
                        element: "##{goombaId}"
                        title: 'This is new survivor'
                        content: "He came to this island with the hope to find a shelter here. He will join jour pride and be it your total control. "
                      }
                      {
                        element: '#whos-turn'
                        title: 'Each pride member have an individual turn with his own points'
                        content: "So with 2 people you can do twice actions a day. But theese people need food and water too. But if they die game won\'t be over. So new survivors will arive shortly."
                      }
                    ],
                    backdrop: true,
                    onEnd: (tour) ->
                      console.log 'tour ended ShowTutorialNewGoomba'
                      Session.set('ShowTutorialNewGoomba', true)
                  )
                  # Initialize the tour
                  tour.init()
                  # Start the tour
                  tour.restart()
              )
            )
          , 0)


        if fields?.turn is 5
          # BUG in meteor https://github.com/meteor/meteor/issues/907#issuecomment-48604374
          setTimeout( ->
            Meteor.call('createGoomba', (
                (error, goombaId) ->
                  if goombaId and Session.get('ShowTutorial') and Session.get('ShowTutorialResource')  and not Session.get('ShowTutorialNewGoomba')
# Instance the tour
                    console.log 'create tour'
                    tour = new Tour(
                      steps: [
                        {
                          element: "##{goombaId}"
                          title: 'This is new survivor'
                          content: "He came to this island with the hope to find a shelter here. He will join jour pride and be it your total control. "
                        }
                        {
                          element: '#whos-turn'
                          title: 'Each pride member have an individual turn with his own points'
                          content: "So with 2 people you can do twice actions a day. But theese people need food and water too. But if they die game won\'t be over. So new survivors will arive shortly."
                        }
                      ],
                      backdrop: true,
                      onEnd: (tour) ->
                        console.log 'tour ended ShowTutorialNewGoomba'
                        Session.set('ShowTutorialNewGoomba', true)
                    )
                    # Initialize the tour
                    tour.init()
                    # Start the tour
                    tour.restart()
              )
            )
          , 0)
#        if fields?.turn is 6
#          console.info 'River is polluted.'
        if fields?.turn is 8
          # BUG in meteor https://github.com/meteor/meteor/issues/907#issuecomment-48604374
          setTimeout( ->
            Meteor.call('createGoomba', (
                (error, goombaId) ->
                  if goombaId and Session.get('ShowTutorial') and Session.get('ShowTutorialResource')  and not Session.get('ShowTutorialNewGoomba')
# Instance the tour
                    console.log 'create tour'
                    tour = new Tour(
                      steps: [
                        {
                          element: "##{goombaId}"
                          title: 'This is new survivor'
                          content: "He came to this island with the hope to find a shelter here. He will join jour pride and be it your total control. "
                        }
                        {
                          element: '#whos-turn'
                          title: 'Each pride member have an individual turn with his own points'
                          content: "So with 2 people you can do twice actions a day. But theese people need food and water too. But if they die game won\'t be over. So new survivors will arive shortly."
                        }
                      ],
                      backdrop: true,
                      onEnd: (tour) ->
                        console.log 'tour ended ShowTutorialNewGoomba'
                        Session.set('ShowTutorialNewGoomba', true)
                    )
                    # Initialize the tour
                    tour.init()
                    # Start the tour
                    tour.restart()
              )
            )
          , 0)
        if fields?.turn is DAYS_TO_SURVIVE
          console.info 'You win.'
          GameSession.update(GameSession.findOne()._id, $set: is_gameover: true)
#          console.info 'Skull hunters arrives.'


Template.gameStatus.helpers
  game: ->
    GameSession.findOne()

  currentGoomba: ->
    gs = GameSession.findOne()
    if gs
      gs.goomba_id




Template.gameManipulatingButtons.events
  'click .enable-tutorial' : (e, template) ->
    Session.set 'ShowTutorial', true
  'click .reset' : (e, template) ->
    e.preventDefault()
    Router.go 'game.menu'
  'click .show-questions' : (e,template) ->
    if Session.get("showQuestions")
      Session.set "showQuestions", false
      $( "#questions" ).dialog( "close" );
    else
      Session.set "showQuestions", true
      $( "#questions" ).dialog().show();
      $('#questions').on('dialogclose', (event) ->
        Session.set "showQuestions", false
      )
    return