Handlebars.registerHelper 'renderGoomba',(_id) ->
    if _id
      el = Goombas.findOne(_id)
      if el and el.name
        el.name

Handlebars.registerHelper 'renderGoombaAvatar',(_id) ->
    if _id
      el = Goombas.findOne(_id)
      if el and el.avatar
        el.avatar

Handlebars.registerHelper 'renderCommand',(_id) ->
    if _id
      el = Actions.findOne(_id)
      if el and el.text
        el.text

Handlebars.registerHelper "add", (x1, x2) ->
  x1 + x2

Handlebars.registerHelper "eq", (x1, x2) ->
  x1 is x2

Handlebars.registerHelper "isCurrentGoomba",  ->
  Session.equals 'currentGoombaId', @_id

Handlebars.registerHelper "isGoombaSelected",  ->
  #TODO вынести в autorun
  gs = GameSession.findOne()
  if gs
    gs.goomba_id == @_id

Handlebars.registerHelper 'dateAgo',(date) ->
    moment(date).fromNow()

Handlebars.registerHelper 'date',(date) ->
  moment(date).format('D.M HH:SS')

Handlebars.registerHelper 'constants',(name) ->
  res =
    PLAYER_LIFE_INITIAL:   PLAYER_LIFE_INITIAL
    PLAYER_HUNGER_INITIAL: PLAYER_HUNGER_INITIAL
    PLAYER_THIRST_INITIAL: PLAYER_THIRST_INITIAL
    HUNGER_PER_DAY:        HUNGER_PER_DAY
    HUNGER_PER_DAY_DAMAGE: HUNGER_PER_DAY_DAMAGE
    THIRST_PER_DAY:        THIRST_PER_DAY
    THIRST_PER_DAY_DAMAGE: THIRST_PER_DAY_DAMAGE
    TURN_POINTS:           TURN_POINTS
    SHOW_TEXT_HINTS:       SHOW_TEXT_HINTS
  res[name]
