Template.resourceManagment.helpers
  items: ->
    Objects.find({in_inventory: true, is_destroyed: false})
  tableSettings: ->
    showFilter: false
    showNavigation: 'auto'
    rowsPerPage: 50
    fields: [
      {
#        key: '_id'
#        label: 'ID'
#      }, {
        key: 'avatar'
        label: 'Icon'
        tmpl: Template.showAvatar
      }, {
        key: 'type'
        label: 'Тип'
      }
    ]
  people: ->
    Goombas.find({health: {$gt: 0}, not_show: {$ne: true}, is_animal: {$ne: true}})
  peopleTableSettings: ->
    showFilter: false
    showNavigation: 'auto'
    rowsPerPage: 50
    fields: [
      {
#        key: '_id'
#        label: 'ID'
#      }, {
        key: 'avatar'
        label: 'Тип'
        tmpl: Template.showAvatar
      }, {
        key: 'name'
        label: 'Имя'
      }, {
        key: 'health'
        label: 'Здоровье'
      }, {
        key: 'hunger'
        label: 'Голод'
      }, {
        key: 'thirst'
        label: 'Жажда'
      }, {
        key: 'is_poisoned'
        label: 'Отравлен'
      }, {
        key: 'type'
        label: 'Действие'
        tmpl: Template.goombaActions
      }
    ]

Template.inventoryCard.helpers
  inventoryGetFreeSpace: ->
    inv = GameSession.findOne()?.inventory_size
    objsInInv = Objects.find({in_inventory: true, is_destroyed: false}).count()
    if inv and objsInInv
      inv - objsInInv
  items: ->
    Objects.find({in_inventory: true, is_destroyed: false})
  tableSettings: ->
    showFilter: false
    showNavigation: 'auto'
    rowsPerPage: 50
    fields: [
      {
#        key: '_id'
#        label: 'ID'
#      }, {
        key: 'avatar'
        label: 'Icon'
        tmpl: Template.showAvatar
      }, {
        key: 'type'
        label: 'Тип'
      }
    ]

Template.removeFromInventoryCard.helpers
  inventoryGetFreeSpace: ->
    inv = GameSession.findOne()?.inventory_size
    objsInInv = Objects.find({in_inventory: true, is_destroyed: false}).count()
    if inv and objsInInv
      inv - objsInInv
  items: ->
    Objects.find({in_inventory: true, is_destroyed: false})
  tableSettings: ->
    showFilter: false
    showNavigation: 'auto'
    rowsPerPage: 50
    fields: [
      {
#        key: '_id'
#        label: 'ID'
#      }, {
        key: 'avatar'
        label: 'Icon'
        tmpl: Template.showAvatar
      }, {
        key: 'type'
        label: 'Тип'
      }, {
        key: 'type'
        label: 'Удалить'
        tmpl: Template.removeObject
      }
    ]


Template.removeObject.events
  'click .remove-from-inventory' : (e,template) ->
    Objects.update @._id, $set: is_destroyed:true
