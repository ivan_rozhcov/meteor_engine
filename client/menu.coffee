Template.menu.events
  'click .create-game' : (e,t) ->
    if not GameSession.findOne()?.is_gameover
      unless confirm('Вы хотите пересоздать игру?')
        return
    Meteor.call('recreateGame')
    console.clear()
    Router.go 'game.grid'

  'click .continue-game' : (e,t) ->
    Router.go 'game.grid'

  'click .join-game' : (e,t) ->
    Router.go 'game.tutorial'
  'click .score' : (e,t) ->
    Router.go 'game.score'


Template.menu.helpers
  canContinue: ->
    not GameSession.findOne()?.is_gameover