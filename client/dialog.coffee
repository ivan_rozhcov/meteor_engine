@parseCommand = (rawCommand) ->
  arr = rawCommand.split(' ')
  command = _.first(arr)
  console.log 'command', command
  act = Actions.findOne(value: command)
  params = _.rest(arr)
  [act._id, act, params]


@executeCommand = (action, params, currentGoomba, time, eventId) ->
  if action.value == 'go'
    param = params[0]
    if param == 'left'
      Goombas.update currentGoomba._id, $inc: x : -30
    else if param == 'right'
      Goombas.update currentGoomba._id, $inc: x : 30
    if param == 'up'
      Goombas.update currentGoomba._id, $inc: y : -30
    else if param == 'down'
      Goombas.update currentGoomba._id, $inc: y : 30
  else if action.value == 'attack'
    if params.length == 2
      param = params[0] + ' ' + params[1]
    else
      param = params[0]
    console.log param
    victim = Goombas.findOne($or: [
      _id: param,
        name: param
    ])
    if victim
      attacker = Goombas.findOne currentGoomba
      Goombas.update victim._id,
        {
          $inc: {health : -DAMAGE_PER_HIT},
          $set: damage_type: "attack by #{attacker.name}"
        }
      Events.update eventId, $set:
        result: success: true
        desc: "#{attacker.name} attacked #{victim.name} by #{DAMAGE_PER_HIT}"

  else if action.value == 'pickup'
    objName  = params[0]
    #найти все предметы в этой клетки
    # TODO в этой клетке!
    obj = Objects.findOne(type: objName, in_inventory:false, is_destroyed: false, x: currentGoomba.x + 15, y: currentGoomba.y + 15)
    #положить в инвертарь
    Goombas.update(currentGoomba._id, $push: inventory: obj._id)
    Objects.update obj._id, $set: in_inventory: true
  else if action.value == 'drop'
    objName  = params[0]
    obj = Objects.findOne(type: objName, in_inventory:true, is_destroyed: false)
    #выкинуть
    Goombas.update(currentGoomba._id, $pop: inventory: obj._id)
    x = currentGoomba.x + 15
    y = currentGoomba.y + 15
    Objects.update obj._id,
      $set:
        in_inventory: false
        x: x
        y: y
  else if action.value == 'say'
    console.log("#{currentGoomba.name} : #{params}")
  else if action.value == 'ask_about_event'
    #ask_about_event seen yzs836cBzYC8qsMv4 what
    #ask_about_event seen AvebhTrMbBRkquQhC null
    knowType = params[0]
    eventId = params[1]
    questionType = params[2]
    suspectGoombaId = params[3]
    suspectGoomba = Goombas.findOne(suspectGoombaId)
    console.log("#{suspectGoomba.name} : #{askAboutEvent(suspectGoomba._id, eventId, questionType, knowType)}")
    Session.set 'typing', suspectGoombaId
    func = () ->
      executeCommand_("say  #{askAboutEvent(suspectGoombaId, eventId, questionType, knowType)}", suspectGoombaId, time)
      $('.panel-body').scrollTo('.chat-message:last');
      Session.set 'typing', null
    Meteor.setTimeout(func, Random.fraction()*1500)
    $('.panel-body').scrollTo('.chat-message:last')
  else if action.value == 'search_island'
    objects = Objects.find(in_inventory: false, is_destroyed: false).fetch()
    if objects
      obj = Random.choice(objects)

      inv = GameSession.findOne()?.inventory_size
      objsInInv = Objects.find({in_inventory: true, is_destroyed: false}).count()
      if inv - objsInInv - 1 < 0
        'У вас нет места в инвентаре, придется что-то удалить'
      Objects.update obj._id, $set: in_inventory: true
      Events.update eventId, $set:
        result:
          object_id: obj._id
          success: true
        desc: "You're found #{obj.type}"
    else
      Events.update eventId, $set:
        result: success: true
        desc: 'You\'re found nothing'
  else if action.value == 'remove_corpse'
    unless params.length == 1
      throw new 'not enough params'
    corpse = Goombas.findOne(params[0])
    if corpse
      Goombas.update(params[0], $set: {x: 8*30, y: 8*30})
      Events.update eventId, $set:
        result: success: true
        desc: "You've moved #{corpse.name} to sea"
  else if action.value == 'catch_an_animal'
    animal = Goombas.findOne({health: {$gt: 0}, is_animal: true, is_catched: {$ne: true}, not_show: {$ne: true}})
    if animal
      Goombas.update(animal._id, $set: {x: 2*30, y: 4*30, is_catched: true})
      Events.update eventId, $set:
        result: success: true
        desc: "You've catch a #{animal.name}"
  else if action.value == 'repair_farm'
    build = Buildings.findOne({health: {$lt: 100}, type: 'ферма'})
    if build
      Buildings.update(build._id, $inc: {health: BUILDING_REPAIR_HEALTH})
      Events.update eventId, $set:
        result: success: true
        desc: "You've repaired a #{build.type}"
  else if action.value == 'repair_house'
    build = Buildings.findOne({health: {$lt: 100}, type: 'дом'})
    if build
      Buildings.update(build._id, $inc: {health: BUILDING_REPAIR_HEALTH})
      Events.update eventId, $set:
        result: success: true
        desc: "You've repaired a #{build.type}"
  else if action.value == 'repair_well'
    build = Buildings.findOne({health: {$lt: 100}, type: 'колодец'})
    if build
      Buildings.update(build._id, $inc: {health: BUILDING_REPAIR_HEALTH})
      Events.update eventId, $set:
        result: success: true
        desc: "You've repaired a #{build.type}"
  else if action.value == 'use_medkit'
    medkit = Objects.findOne(type: 'аптечка', in_inventory: true, is_destroyed: false)
    if medkit
      Goombas.update currentGoomba._id, {$inc: {health : HEALTH_FROM_MEDKIT}, $set: is_poisoned: false}
      Objects.update medkit._id, $set: is_destroyed:true
      Events.update eventId, $set:
        result: success: true
        desc: "You've healed a #{medkit.type} by #{HEALTH_FROM_MEDKIT}"
    else
      Events.update eventId, $set:
        result: success: false
        desc: "You don't have a medkit!"
  else if action.value == 'eat_food'
    medkit = Objects.findOne(type: 'еда', in_inventory: true, is_destroyed: false)
    if medkit
      Goombas.update currentGoomba._id, $inc: {hunger : HUNGER_FROM_FOOD, health: HEALTH_FROM_FOOD}
      Objects.update medkit._id, $set: is_destroyed:true
      Events.update eventId, $set:
        result: success: true
        desc: "You've had a meal (hunger raised by #{HUNGER_FROM_FOOD})"
    else
      Events.update eventId, $set:
        result: success: false
        desc: "You don't have food!"
  else if action.value == 'drink_water'
    medkit = Objects.findOne(type: 'вода', in_inventory: true, is_destroyed: false)
    if medkit
      Goombas.update currentGoomba._id, $inc: {thirst : THIRST_FROM_WATER, health: HEALTH_FROM_WATER}
      Objects.update medkit._id, $set: is_destroyed:true
      Events.update eventId, $set:
        result: success: true
        desc: "You've drink a water (thirst raised by #{THIRST_FROM_WATER}) (health is raised by #{HEALTH_FROM_WATER})"
    else
      Events.update eventId, $set:
        result: success: false
        desc: "You don't have water!"

  else if action.value == 'get_water_from_river'
    water = Objects.insert(
      type: 'вода'
      in_inventory: true
      is_destroyed: false
      avatar: 'http://icons.veryicon.com/256/System/Line/Glass%20Water.png'
    )
    Events.update eventId, $set:
      result: success: true
      desc: "Get water from a river"

  else if action.value == 'get_water_from_well'
    water = Objects.insert(
      type: 'вода'
      in_inventory: true
      is_destroyed: false
      avatar: 'http://icons.veryicon.com/256/System/Line/Glass%20Water.png'
    )
    Events.update eventId, $set:
      result: success: true
      desc: "Get water from a well"

  else if action.value == 'cook'
    unless params.length == 1
      throw new 'not enough params'
    corpse = Goombas.findOne(params[0])
    if corpse
      Goombas.update(params[0], $set: not_show: true)
      if corpse.is_animal
        Objects.insert(
          type: 'еда'
          in_inventory: true
          is_destroyed: false
          avatar: 'https://www.shareicon.net/data/128x128/2016/09/26/834904_food_512x512.png'
        )
        meat = 1
      #if it's a human get 2 meats
      else
        Objects.insert(
          type: 'еда'
          in_inventory: true
          is_destroyed: false
          avatar: 'https://www.shareicon.net/data/128x128/2016/09/26/834904_food_512x512.png'
        )
        Objects.insert(
          type: 'еда'
          in_inventory: true
          is_destroyed: false
          avatar: 'https://www.shareicon.net/data/128x128/2016/09/26/834904_food_512x512.png'
        )
        meat = 2
      Events.update eventId, $set:
        result: success: true
        desc: "Got #{meat} еда from #{corpse.name}"
  else if action.value == 'hunt'
    Objects.insert(
      type: 'еда'
      in_inventory: true
      is_destroyed: false
      avatar: 'https://www.shareicon.net/data/128x128/2016/09/26/834904_food_512x512.png'
    )
    Events.update eventId, $set:
      result: success: true
      desc: "Got 1 meat from wild animal"
  else if action.value == 'decrease_thirst'
    Goombas.update currentGoomba._id, $inc: {thirst : -THIRST_BAD_DREAM}
    Events.update eventId, $set:
      result: success: true
      desc: "decrease_thirst"
  else if action.value == 'gone_mad'
    Events.update eventId, $set:
      result: success: true
      desc: "gone_mad"



@executeCommand_ = (rawCommand, currentGoombaId, time, turn) ->
  [commandId, command, params] = parseCommand(rawCommand)
  console.log commandId, command, params
  currentGoomba = Goombas.findOne(currentGoombaId)

  histId = Events.insert(
    create_time: time,
    who: currentGoombaId,
    command_id: commandId,
    params: params,
    where: [currentGoomba.x, currentGoomba.y],
    turn: turn
  )

  executeCommand(command, params, currentGoomba, time, histId)

  afterTurnGoomba = Goombas.findOne(currentGoombaId)
  #вставляем событие - TODO по хорошему через хуки
  #видит
  for g in Goombas.find(x: afterTurnGoomba.x, y: afterTurnGoomba.y, health: $gt: 0).fetch()
    Goombas.update g._id, $push: 'kb.seen': {eventId: histId, time: time}

  #слышит
  for g in Goombas.find($or: [
    x: afterTurnGoomba.x + 30, y: afterTurnGoomba.y,
      x: afterTurnGoomba.x - 30, y: afterTurnGoomba.y,
    x: afterTurnGoomba.x, y: afterTurnGoomba.y + 30,
      x: afterTurnGoomba.x, y: afterTurnGoomba.y - 30,
  ],health: $gt: 0).fetch()
    Goombas.update g._id, $push: 'kb.heard': {eventId: histId, time: time}

Template.createButtons.helpers
  actionAvaible: ->
    not ($('.description').val() is null)

Template.createButtons.events
  'click .show-inventory' : (e,template) ->
    $( "#inventory" ).dialog(
      minWidth: 636
      title: 'Инвентарь'
    ).show()

  'click .send' : (e,template) ->
    gs = GameSession.findOne()
    turn = gs.turn
    time = new Date().getTime()

    rawCommand = $('.description').val()
    action = Actions.findOne(value: rawCommand)
    if action.cost + gs.spend_points > TURN_POINTS
      alert 'this action is too expensive'
      return

    currentGoombaId = gs.goomba_id
    if rawCommand and currentGoombaId
      if action.context
        #TODO check
        param = $('.action-context').val()
        unless param
          param = 'any'
        rawCommandWithParams = rawCommand + ' ' + param
      else
        rawCommandWithParams = rawCommand
      executeCommand_(rawCommandWithParams, currentGoombaId, time, gs.turn)
      $('.description').val('')
      $('.description').trigger('change')

      $('.action-context').val('')
      $('.action-context').trigger('change')

      GameSession.update( gs._id , $inc: spend_points: action.cost)


  'click .make-turn' : (e,template) ->
    console.log 'make turn'
    goombaId = null
    gs = GameSession.findOne()
    goombasIds = Goombas.find( health: {$gt: 0}, is_animal: {$ne: true}, not_show: {$ne: true} ).map( (s) -> s._id)
    if not gs.goomba_id
      goombaId = goombasIds[0]
    else
      #ищем индекс в массиве
      index = _.indexOf(goombasIds, gs.goomba_id)
      index += 1
      if index != goombasIds.length
        goombaId = goombasIds[index]
      else
        #все походили - перееходим на следующий ход
        GameSession.update gs._id,
          $inc: turn: 1
          $unset: {goomba_id: 1, spend_points: 1}
        Meteor.call('updateHungerAndThirst')
        Meteor.call('drownObject')
        Meteor.call('breedAnimalsOnTheWild')
        Meteor.call('breedAnimalsOnTheFarm')
        Meteor.call('corpseDecay')
        Meteor.call('diseaseSpread')
        gs = GameSession.findOne()
        goombasIds = Goombas.find( health: {$gt: 0}, is_animal: {$ne: true}, not_show: {$ne: true} ).map( (s) -> s._id)
        if not gs.goomba_id
          goombaId = goombasIds[0]

        Meteor.call('getRandomEvents',
          (error, args) ->
            [randomEvent, object] = args
            if randomEvent
              console.log('event', randomEvent, object)
              Session.set "eventForDialog", randomEvent
              Session.set "objectForDialog", object

              turn = gs.turn
              time = new Date().getTime()
              # action will take effect
              executeCommand_(randomEvent.action, object._id, time, turn)

              $( "#eventCard" ).dialog(
                minWidth: 720
                title: "Новое событие! #{randomEvent.title}"
                modal: true
                close: ( event, ui ) ->
                  console.log event, ui

                  $( "#resource-managment" ).dialog(
                    minWidth: 720
                    title: 'Распределение ресурсов'
                    modal: true
                  ).show()
                  console.log 'create tour', Session.get('ShowTutorial'), Session.get('ShowTutorial') and not Session.get('ShowTutorialResource')
                  if Session.get('ShowTutorial') and not Session.get('ShowTutorialResource')
                    # Instance the tour
                    console.log 'create tour'
                    tour = new Tour(
                      steps: [
                        {
                          element: '#resource-managment'
                          title: 'This is resource managment screen'
                          content: "It appears at the start of each day."
                        }

                        {
                          element: '#reactive-table-8'
                          title: 'This is all your items.'
                          content: 'Your can find new item on island.'
                        }

                        {
                          element: '#reactive-table-9'
                          title: 'This is your people'
                          content: 'Including you. If player dies game will be over. '
                        }

                        {
                          element: '#resource-managment td.health'
                          title: 'It\'s a health of person'
                          content: 'People die when their health reach 0 level. Reasons can be different: hunger, thirst, poison, attack...'
                        }

                        {
                          element: '#resource-managment td.hunger'
                          title: 'It\'s a hunger level of person'
                          content: "Hunger level decrease every day by #{HUNGER_PER_DAY}. If hunger level reached 0 it will damage person's health by #{HUNGER_PER_DAY_DAMAGE} every day."
                        }

                        {
                          element: '#resource-managment td.thirst'
                          title: 'It\'s a thirst level of person'
                          content: "Hunger level decrease every day by #{THIRST_PER_DAY}. If thirst level reached 0 it will damage person's health by #{THIRST_PER_DAY_DAMAGE} every day."
                        }

                        {
                          element: '#resource-managment td.type'
                          title: 'It\'s a list of actions'
                          content: 'You can apply each action for every person of you pride. It won\'t cost you athything. You can also take this item during turn (for a cost of poinst)'
                        }

                        {
                          element: '#resource-managment td.type img.feed'
                          title: 'You can feed this person'
                          content: 'If you have food.'
                        }

                        {
                          element: '#resource-managment td.type img.drink'
                          title: 'You can feed this person'
                          content: 'If you have water.'
                        }

                        {
                          element: '#resource-managment td.type img.heal'
                          title: 'You can heal this person'
                          content: 'If you have medkit.'
                        }

                        {
                          element: '.ui-dialog-titlebar-close'
                          title: 'When you finish resource managment you can close this dialog'
                          content: 'This mean you will start your day. You\'ll see this screen on the start of the next day'
                        }
                      ],
                      backdrop: true,
                      onEnd: (tour) ->
                        console.log 'tour ended ShowTutorialResource'
                        Session.set('ShowTutorialResource', true)
                    )
                    # Initialize the tour
                    tour.init()
                    # Start the tour
                    tour.restart()



              ).show()

              turn = GameSession.findOne()?.turn
              Events.insert
                create_time: new Date().getTime(),
                desc: 'new day'
                turn: turn

            else
              Session.set('ShowTutorialResource', true)

        )


    if goombaId
      GameSession.update(gs._id, {$set: {goomba_id: goombaId},  $unset: {spend_points: 1}})


Template.journal.events
  'click .command' : (e,template) ->
    $('.description').val(Actions.findOne(@command_id).text + ' ' +  @params.join(' '))


Template.input.onRendered ->
  $(".description").select2({
    allowClear: true,
    placeholder: "",
  })
  $(".action-context").select2({
    allowClear: true,
    placeholder: "",
  })

Template.input.helpers
  available_actions: ->
    simpleActions = Actions.find(condition: $exists: false).fetch()
    for action in Actions.find(condition: $exists: true).fetch()
      if eval action.condition
        simpleActions.push(action)
    simpleActions
  available_actions_context: ->
    Session.get 'action_contex'

Template.input.events
  'select2:select .description' : (e, template) ->
    target = $(e.currentTarget).val()
    console.log(target)
    action = Actions.findOne(value: target)
    console.log action
    console.log action, action.context, eval(action.context)
    evaledContext = eval action.context
    if evaledContext
      if action.exclude_self
        goombaId = GameSession.findOne()?.goomba_id
        if goombaId
          context = _.filter(evaledContext, (itm) -> itm._id != goombaId)
        else
          context = eval(evaledContext)
      else
        context = eval(evaledContext)
      console.log 'context', context
      Session.set 'action_contex', context
      $(".action-context").select2({
        allowClear: true,
        placeholder: "",
      })
    else
      Session.set 'action_contex', null

  'select2:unselect .description' : (e, template) ->
    Session.set 'action_contex', null
#  #TODO fix scroll issue
#  'onscroll body' : (e, template) ->
#    console.log 'scroll'

Template.journal.helpers
  items: ->
    Events.find()
  tableSettings: ->
    showFilter: false
    showNavigation: 'auto'
    rowsPerPage: 50
    fields: [
      {
#        key: '_id'
#        label: 'ID'
#      }, {
        key: 'create_time'
        label: 'Время'
        tmpl: Template.dateAgoCreateTime,
        sortOrder: 1,
        sortDirection: 'descending'
      }, {
        key: 'turn'
        label: 'Ход'
      }, {
        key: 'who'
        label: 'Кто'
        tmpl: Template.renderGoomba
      }, {
        key: 'command_id'
        label: 'Команда'
        tmpl: Template.renderCommand
#      }, {
#        key: 'params'
#        label: 'Параметры'
      }, {
        key: 'desc'
        label: 'Описание'
      }
    ]