Template.buildingCard.helpers
  currentBuilding: ->
    Buildings.findOne(Session.get 'buildingForDialog')

Template.buildings.helpers
  items: ->
    Buildings.find()
  tableSettings: ->
    showFilter: false
    showNavigation: 'auto'
    rowsPerPage: 50
    fields: [
      {
#        key: '_id'
#        label: 'ID'
#      }, {
        key: 'type'
        label: 'Тип'
#      }, {
#        key: 'personality'
#        label: 'Позиция'
#        tmpl: Template.showPosition
#      }, {
#        key: 'lives'
#        label: 'Живут'
#        tmpl: Template.showLives
#      }, {
#        key: 'works'
#        label: 'Работают'
#        tmpl: Template.showWorks
      }, {
        key: 'health'
        label: 'Состояние'
      }
    ]