Template.goombaCard.helpers
  currentGoomba: ->
    Goombas.findOne(Session.get 'goombaForDialog')
  isDead: ->
    @health <= 0

Template.goombas.helpers
  items: ->
    filter = {}
    if ! Session.get('show_dead')
      filter.health = {$gt: 0}
    if ! Session.get('show_animals')
      filter.is_animal = {$ne: true}
    if ! Session.get('show_all')
      filter.not_show = {$ne: true}
    Goombas.find(filter)
  tableSettings: ->
    rowClass: (itm) ->
      gs = GameSession.findOne()
      if gs and gs.goomba_id
        if gs.goomba_id == itm._id
          'active-row'
        else if itm.health < 1
          'dead'

    showFilter: false
    showNavigation: 'auto'
    rowsPerPage: 50
    fields: [
      {
#        key: '_id'
#        label: 'ID'
#      }, {
        key: 'avatar'
        label: 'Аватар'
        tmpl: Template.showAvatar
      }, {
        key: 'name'
        label: 'Имя'
#      }, {
#        key: 'occupation'
#        label: 'Профессия'
#      }, {
#        key: 'personality'
#        label: 'Тип личности'
#      }, {
#        key: 'aligment'
#        label: 'Тип'
      }, {
        key: 'sex'
        label: 'Пол'
#      }, {
#        key: 'personality'
#        label: 'Позиция'
#        tmpl: Template.showPosition
      }, {
        key: 'health'
        label: 'Здоровье'
      }, {
        key: 'hunger'
        label: 'Голод'
      }, {
        key: 'thirst'
        label: 'Жажда'
      }, {
        key: 'is_decayed'
        label: 'Разложился'
      }, {
        key: 'is_poisoned'
        label: 'Отравлен'
      }
    ]

Template.goombas.events
  'click #show_dead' : (e, template) ->
    Session.set('show_dead', e.currentTarget.checked)
  'click #show_animals' : (e, template) ->
    Session.set('show_animals', e.currentTarget.checked)
  'click #show_all' : (e, template) ->
    Session.set('show_all', e.currentTarget.checked)

processInventory = (type, name, goombaId, command) ->
  healthKit = Objects.findOne(type: type, in_inventory: true, is_destroyed: false)
  unless healthKit
    alert "You don\'t have a #{name}!"
    return
  turn = GameSession.findOne()?.turn
  time = new Date().getTime()
  executeCommand_(command, goombaId, time, turn)

Template.goombaActions.events
  'click .heal' : (e,template) ->
    processInventory('аптечка', 'health kit', @_id, 'use_medkit')
  'click .feed' : (e,template) ->
    processInventory('еда', 'food', @_id, 'eat_food')
  'click .drink' : (e,template) ->
    processInventory('вода', 'water', @_id, 'drink_water')